<?php
if (isset($_POST['inputVar'])) {
  $num = $_POST['inputVar'];

  $redis = new Redis();
  connectRedis('localhost', 6379);

  // if (false) {
  if ($redis->exists($num)) {
    printResult(getFromCache($num));
  } else {
    printResult(calcFac($num));
  }


}

function getFromCache($num) {
  global $redis;
  return $redis->get($num); 
}

function calcFac($num) {
  global $redis;
  $returnVar = 0;
  for($i = 1; $i<$num; $i++) {
    $returnVar += $num*$i;
  }
  $redis->set($num, $returnVar);
  sleep(2);
  return $returnVar;
  
}

function printResult($fac) {
  echo "<h3>Result: $fac</h3>";
}

function connectRedis($hostname, $port){
  global $redis;
  try {
    $connected = $redis->connect($hostname, $port);

    if (!$connected) {
      throw new Exception();
    }

  } catch (Exception $e) {
    die($e->getMessage());
  
  }
}

?>
<!DOCTYPE html>
<html>
<body>
<h2>Calcula Factorial</h2>
<form method="post" action="no_recursiu.php">
  <input type="text" id="inputVar" name="inputVar" placeholder="Introdueïx el nombre"><br>
  <input type="submit" value="Submit">
</form>
</body>
</html>