<?php
if (isset($_POST['inputVar'])) {
  $num = $_POST['inputVar'];

  $redis = new Redis();
  connectRedis('localhost', 6379);

  printResult(calcFac($num));


}

function printResult($fac) {
  echo "<h3>Result: $fac</h3>";
}

function calcFac($num) {
  global $redis;
  if ($num==0) return 1;
  if ($redis->exists($num)) {
    return $redis->get($num);
  }
  sleep(2);
  $returnVar = calcFac($num-1) * $num;
  $redis->set($returnVar, $num);
  return $returnVar;
  
}

function connectRedis($hostname, $port){
  global $redis;
  try {
    $connected = $redis->connect($hostname, $port);

    if (!$connected) {
      throw new Exception();
    }

  } catch (Exception $e) {
    die($e->getMessage());
  
  }
}

?>
<!DOCTYPE html>
<html>
<body>
<h2>Calcula Factorial</h2>
<form method="post" action="recursiu.php">
  <input type="text" id="inputVar" name="inputVar" placeholder="Introdueïx el nombre"><br>
  <input type="submit" value="Submit">
</form>
</body>
</html>